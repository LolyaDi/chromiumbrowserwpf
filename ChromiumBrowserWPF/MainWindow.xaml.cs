﻿using CefSharp;
using CefSharp.Wpf;
using EO.Wpf;
using System.Windows;

namespace ChromiumBrowserWPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const string DEFAULT_URL = "https://www.google.com";

        private ChromiumWebBrowser _chromium;

        public MainWindow()
        {
            System.Diagnostics.PresentationTraceSources.DataBindingSource.Switch.Level = System.Diagnostics.SourceLevels.Critical;

            InitializeComponent();

            Cef.Initialize(new CefSettings());

            var tabItem = InitializeStartUpItem();
            browserTabControl.Items.Add(tabItem);
        }

        private TabItem InitializeStartUpItem()
        {
            _chromium = new ChromiumWebBrowser(DEFAULT_URL);

            return new TabItem
            {
                Header = "Google",
                Content = _chromium
            };
        }

        private void BrowserTabsNewItemAdded(object sender, TabItemEventArgs e)
        {
            var tabItem = InitializeStartUpItem();

            if (!e.Item.HasContent)
            {
                e.Item.Header = tabItem.Header;
                e.Item.Content = tabItem.Content;
            }
        }

        private void MainWindowClosed(object sender, System.EventArgs e)
        {
            Cef.Shutdown();
        }
    }
}
